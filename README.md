# CommitKeywords README

Welcome to the README for the "commitKeywords" extension! This extension is designed to streamline your Git workflow and enhance your Continuous Integration (CI) pipeline by simplifying the process of triggering CI actions with specific keywords in your commit messages.

## Features

- **Streamlined CI Workflow:** CommitKeywords makes it easy to trigger CI pipeline actions by using specific keywords in your commit messages.
- **Effortless Integration:** Seamlessly integrate this extension into your VS Code environment to enhance your Git workflow.
- **Customizable Settings:** Tailor the extension to your needs with customizable settings.
- **Shared Keywords via Git:** Collaborate with your team by storing and sharing keywords through Git using a JSON configuration file.


<p align="center">
  <img src="./images/demo.gif" alt="Example GIF">
</p>

## Requirements

Before you get started, ensure you have the following requirements:

- [Visual Studio Code](https://code.visualstudio.com/) (obviously)
- Your Git repository configured for CI with specific keywords (See `.vscode/commitKeywords` for the keyword configuration)

## Extension commands

This extension contributes the following commands, which can be used in your VS Code:

- `extension.commitKeywords`: trigger the list of keywords if the file does not exist will generate one with a default value.

## Known Issues

If you encounter any issues while using this extension, please check our [GitLab Issues](https://gitlab.com/CaptainPickles/commitkeywords/-/issues) for known issues, or open a new one if it hasn't been reported yet.

## Release Notes

Stay up-to-date with the latest changes and improvements by checking our [Release Notes](https://gitlab.com/CaptainPickles/commitkeywords/-/tags).

### 1.0.0

- Initial release of "commitKeywords" extension.

## Following Extension Guidelines

We've made sure to follow the Visual Studio Code extension guidelines to provide a smooth experience for our users. Please review these guidelines for more information: [Extension Guidelines](https://code.visualstudio.com/api/references/extension-guidelines).

**Enjoy your improved Git workflow with "commitKeywords"!**
