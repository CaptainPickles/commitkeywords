import { QuickInputButton, QuickPickItem, QuickPickItemKind, ThemeIcon, Uri } from "vscode"

export interface CommitKeywords {
label:string
description?:string,
value:string
}

export interface CommitKeywordsConf {
    keywords: CommitKeywords[]
}

export interface CommitKeywordsQuickPick  extends QuickPickItem,  Pick<CommitKeywords, "value"> {
label: string
kind?: QuickPickItemKind | undefined
iconPath?: Uri | ThemeIcon | { light: Uri; dark: Uri } | undefined
description?: string | undefined
detail?: string | undefined
picked?: boolean | undefined
alwaysShow?: boolean | undefined
buttons?: readonly QuickInputButton[] | undefined
value :string
}