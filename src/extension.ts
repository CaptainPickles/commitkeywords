// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import { GitExtension, Repository } from "./api/git";
import { configFileName } from "./constants";
import {
  CommitKeywordsConf,
  CommitKeywordsQuickPick,
} from "./api/commitKeywords";

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export async function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log(
    'Congratulations, your extension "commitKeywords" is now active!'
  );

  let disposable = vscode.commands.registerCommand(
    "extension.commitKeywords",
    async () => {
      const git = getGitExtension();
      if (!git) {
        await vscode.window.showErrorMessage("unable to load Git Extension");
        return;
      }
      try {
        if (vscode.workspace.workspaceFolders === undefined) {
          await vscode.window.showErrorMessage("no workspace folder");
          return;
        }
        const wsPath = vscode.workspace.workspaceFolders[0].uri.fsPath; // gets the path of the first workspace folder
        const filePath = vscode.Uri.file(wsPath + configFileName);
        const conf = await readJsonConfOrCreateIfNotExist(filePath);
        const quickPickItems: CommitKeywordsQuickPick[] = [];
        for (const keyword of conf.keywords) {
          quickPickItems.push({
            label: keyword.label,
            detail: keyword.description,
            value: keyword.value,
            description: keyword.value,
          });
        }
        vscode.window.showQuickPick(quickPickItems).then(function (selected?) {
          if (selected) {
            vscode.commands.executeCommand("workbench.view.scm");
            for (let repo of git.repositories) {
              prefixCommit(repo, selected.value);
            }
          }
        });
      } catch (error) {
        console.log("error", error);
        vscode.window.showErrorMessage(
          "unable to load commitKeywords config file"
        );
        return;
      }
    }
  );

  context.subscriptions.push(disposable);
}

function prefixCommit(repository: Repository, prefix: String) {
  repository.inputBox.value = `${prefix} ${repository.inputBox.value}`;
}

function getGitExtension() {
  const vscodeGit = vscode.extensions.getExtension<GitExtension>("vscode.git");
  const gitExtension = vscodeGit && vscodeGit.exports;
  return gitExtension && gitExtension.getAPI(1);
}

function isCommitKeywordsConf(json: any): json is CommitKeywordsConf {
  if (json.keywords) {
    for (const keyword of json.keywords) {
      if (!keyword.label || !keyword.description || !keyword.value) {
        return false;
      }
    }
    return true;
  }
  return false;
}

async function readJsonConfOrCreateIfNotExist(
  filePath: vscode.Uri
): Promise<CommitKeywordsConf> {
  let stringifiedData: string = "";
  try {
    const document = await vscode.workspace.openTextDocument(filePath);
    stringifiedData = document.getText();
  } catch (error) {
    stringifiedData = await createDefaultConfFile();
  }
  try {
    const parsedData = JSON.parse(stringifiedData);
    if (!isCommitKeywordsConf(parsedData)) {
      throw new Error("invalid commitKeywords.json");
    }
    if (parsedData.keywords.length === 0) {
      await vscode.window.showInformationMessage(
        "Please defined at least one keyword in your commitKeywords.json file"
      );
    }
    return parsedData;
  } catch (error) {
    throw new Error("invalid commitKeywords.json");
  }
}
/**
 * @returns stringified default json content
 */
async function createDefaultConfFile(): Promise<string> {
  const wsedit = new vscode.WorkspaceEdit();
  if (vscode.workspace.workspaceFolders === undefined) {
    throw new Error("no workspace folder");
  }
  const wsPath = vscode.workspace.workspaceFolders[0].uri.fsPath; // gets the path of the first workspace folder
  const filePath = vscode.Uri.file(wsPath + configFileName);
  const defaultConf: CommitKeywordsConf = getDefaultConf();
  const stringifiedDefaultConf = JSON.stringify(defaultConf);
  wsedit.createFile(filePath, {
    ignoreIfExists: true,
    contents: Buffer.from(stringifiedDefaultConf),
  });
  await vscode.workspace.applyEdit(wsedit);
  await vscode.window.showInformationMessage(
    "Default configuration file created at path" + configFileName,
    {
      modal: true,
      detail:
        "You can customize the configuration by editing the newly created file.",
    }
  );
  const document = await vscode.workspace.openTextDocument(filePath);
  await vscode.window.showTextDocument(document);
  return stringifiedDefaultConf;
}

function getDefaultConf(): CommitKeywordsConf {
  return {
    keywords: [
      {
        label: "🚀 Initial Commit",
        value: "Initial",
        description:
          "Welcome to CommitKey! This is your first keyword, and it's here to get you started with your initial commit 🎉. You can customize its description, label, and, of course, the value for your commit in " +
          configFileName +
          ". Use it as a template to simplify and standardize your commit messages.",
      },
    ],
  };
}

// This method is called when your extension is deactivated
export function deactivate() {}
